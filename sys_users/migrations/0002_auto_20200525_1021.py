# Generated by Django 3.0.6 on 2020-05-25 10:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('virtual', '0011_auto_20200524_1233'),
        ('sys_users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='domainadmin',
            name='domain',
        ),
        migrations.AddField(
            model_name='company',
            name='domain',
            field=models.ManyToManyField(to='virtual.Domain'),
        ),
    ]
