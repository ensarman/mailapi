# Generated by Django 3.0.6 on 2020-05-23 07:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('virtual', '0007_auto_20200523_0658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='password',
            field=models.CharField(default=None, max_length=100),
        ),
    ]
